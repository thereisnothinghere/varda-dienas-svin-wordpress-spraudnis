# Vārda dienas svin (Wordpress spraudnis)

Mācību nolūkos ar PHP izveidoju Wordpress spraudni, kas izvada vārda dienas jubilārus.

Pēc kā papildināju ar funkcionalitāti, ka tiek ierakstīti draugu vārdi un kad ir viņu dzimšanas diena, tiek izvadīts atsevišķs paziņojums mājas lapā.

Pēc spraudņa aktivizēšanas sadaļā (Widgets) ir iespēja pievienot spraudni mājas lapas blokā.
Ja vēlies saņemt konkrētu paziņojumu par drauga vārdadienu, ievades laukā "Tavi draugu vārdi" ir jāsaglabā sekojošais ievades formāts "vārds, vārds, ...".

Uz doto brīdi ir divas iespējas, izvadīt paziņojumu tikai tad, kad kādam no ievadītajiem vārdiem ir vārda diena. Un atzīmējot checkbox "	Izvadīt citu cilvēku vārda dienas?" tiks izvadīts attiecīgās dienas jubilāri.